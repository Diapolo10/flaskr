from pyfladesk import init_gui
from flaskr import create_app

app = create_app()

APP_WIDTH = 720
APP_HEIGHT = 640
WINDOW_TITLE = "FLASKR - The #1 Twitter clone"

if __name__ == '__main__':
    init_gui(
        app,
        port         = "8080",
        width        = APP_WIDTH,
        height       = APP_HEIGHT,
        window_title = WINDOW_TITLE,
    )
